let {Lexer, Parser} = require('../pkg/wasm_toy_lang');

Parser[Symbol.iterator] = () => {
    console.log('next', this)
    return { next: this.get_next_raw_statement }
}


let lexer = Lexer.new(`
let x = "Hello World!"; 
let y = 0.3; 
let z = 12;`);

let parser = Parser.new(lexer);

console.log(parser.get_next_raw_statement())
console.log(parser.get_next_raw_statement())
console.log(parser.get_next_raw_statement())
console.log(parser.get_next_raw_statement())
console.log(parser.get_next_raw_statement())
console.log(parser.get_next_raw_statement())
console.log(parser.get_next_raw_statement())