//! Test suite for the Web and headless browsers.

#![cfg(target_arch = "wasm32")]

extern crate core;
extern crate wasm_bindgen_test;

use wasm_bindgen_test::*;
use wasm_toy_lang::evaluable::Evaluable;
use wasm_toy_lang::lexer::Lexer;
use wasm_toy_lang::parser::Parser;
use wasm_toy_lang::token::{IdentifierToken, Token};
wasm_bindgen_test_configure!(run_in_browser);
struct TestEvaluable;

impl Evaluable<String, ()> for TestEvaluable {
    fn eval(&self) -> Result<String, ()> {
        Ok("Hello World!".to_owned())
    }
}

#[wasm_bindgen_test]
fn test_eval() {
    let x = TestEvaluable;
    let res = x.eval().unwrap();
    assert_eq!(res, "Hello World!".to_owned())
}

#[wasm_bindgen_test]
fn lexer() {
    let mut lexer = Lexer::new("hello_world = 30;");

    let token = lexer.next().unwrap();
    match token {
        Token::Identifier(IdentifierToken { name }) => assert_eq!(name, "hello_world".to_string()),
        _ => {}
    }
}
// #[wasm_bindgen_test]
// fn parser () {
//     Parser::parse(Lexer::new("1 + 1"));
// }
