pub trait Evaluable<T, E> {
    fn eval(&self) -> Result<T, E>;
}
