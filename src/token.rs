use crate::wasm_bindgen;
use serde::{Deserialize, Serialize};
use std::ops::Add;
use wasm_bindgen::JsValue;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type", content = "body")]
pub enum NumberToken {
    Integer(i64),
    Float(f64),
}

impl<'a> From<&'a str> for NumberToken {
    fn from(value: &'a str) -> Self {
        let contains_delimiter = value.contains(".") || value.contains(",");
        if !contains_delimiter {
            return NumberToken::Integer(value.parse::<i64>().unwrap());
        }
        NumberToken::Float(value.parse::<f64>().unwrap())
    }
}

impl Add for NumberToken {
    type Output = f64;
    fn add(self, rhs: Self) -> Self::Output {
        let left_num = match self {
            Self::Integer(inner) => inner as f64,
            Self::Float(inner) => inner,
        };
        let right_num = match rhs {
            Self::Integer(inner) => inner as f64,
            Self::Float(inner) => inner,
        };
        left_num + right_num
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type", content = "body")]
pub enum DelimiterToken {
    Space,
    Period,
    Comma,
    SingleQuote,
    DoubleQuote,
    BackTick,
    OpenParenthesis,
    CloseParenthesis,
    OpenBracket,
    CloseBracket,
    OpenBrace,
    CloseBrace,
    Colon,
    SemiColon,
    EOF,
    NewLine,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type", content = "body")]
pub enum LiteralToken {
    Number(NumberToken),
    String(String),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[wasm_bindgen]
#[serde(tag = "type", content = "body")]
pub enum OperatorToken {
    Plus,
    Minus,
    Multiply,
    Divide,
    Or,
    And,
    Not,
    Assign,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct IdentifierToken {
    pub name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type", content = "body")]
pub enum KeywordToken {
    Let,
    Fn,
    Unknown,
}
impl From<String> for KeywordToken {
    fn from(value: String) -> Self {
        match value.as_str() {
            "let" => KeywordToken::Let,
            "fn" => KeywordToken::Fn,
            _ => KeywordToken::Unknown,
        }
    }
}
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type", content = "body")]
pub enum Token {
    Operator(OperatorToken),
    Delimiter(DelimiterToken),
    Identifier(IdentifierToken),
    Literal(LiteralToken),
    Keywords(KeywordToken),
    Unknown(char),
    End,
}

impl Into<JsValue> for &mut Token {
    fn into(self) -> JsValue {
        serde_wasm_bindgen::to_value(self).unwrap()
    }
}