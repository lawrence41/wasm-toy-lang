#![feature(iter_collect_into)]

pub mod ast;
pub mod evaluable;
pub mod lexer;
pub mod token;

mod keywords;
pub mod parser;
mod utils;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
