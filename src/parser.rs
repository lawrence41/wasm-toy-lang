use std::borrow::BorrowMut;
use wasm_bindgen::JsValue;
use crate::ast::ASTNode;
use crate::lexer::Lexer;
use crate::token::{DelimiterToken, Token};
use crate::wasm_bindgen;


#[wasm_bindgen]
pub struct Parser {
    ast: Vec<ASTNode>,
    current_statement: Option<ASTNode>,
    last_token: Option<Token>,
    lexer: Lexer
}
#[wasm_bindgen]
impl Parser {
    pub fn new (lexer: Lexer) -> Self {
        Self {
            lexer,
            last_token: None,
            current_statement: None,
            ast: vec![]
        }
    }
    pub fn get_next_raw_statement(&mut self) -> JsValue {
        let mut raw_statement = vec![];
        let mut end = false;
        for token in self.lexer.borrow_mut() {
            if end { break; }
            match token {
                Token::End => end = true,
                Token::Delimiter(DelimiterToken::SemiColon) => end = true,
                Token::Delimiter(DelimiterToken::Space) => {},
                _ => raw_statement.push(token)
            }
        }
        serde_wasm_bindgen::to_value(&raw_statement.iter().cloned().collect::<Vec<Token>>()).unwrap()
    }

}