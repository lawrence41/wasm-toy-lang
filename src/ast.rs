use crate::evaluable::Evaluable;
use crate::token::{IdentifierToken, LiteralToken, OperatorToken, Token};

pub struct VariableDeclarator {
    id: IdentifierToken,
    init: Token,
}
pub struct VariableDeclaration {
    declarations: Vec<VariableDeclarator>,
}

pub enum Expression {
    Literal(LiteralToken),
    Binary(Box<Expression>, Box<Expression>)
}

pub struct BinaryExpression {
    operator: OperatorToken,
    left: Box<ASTNode>,
    right: Box<ASTNode>
}

pub enum ASTNode {
    VariableDeclaration(VariableDeclaration),
    ExpressionStatement(Expression),
    LiteralStatement(LiteralToken),
    Branch(Box<ASTNode>)
}
