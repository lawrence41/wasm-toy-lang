use crate::token::{
    DelimiterToken, IdentifierToken, KeywordToken, LiteralToken, NumberToken, OperatorToken, Token,
};
use serde::Serialize;

use crate::keywords::KEYWORDS;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

impl Iterator for Lexer {
    type Item = Token;
    fn next(&mut self) -> Option<Self::Item> {
        if self.start == self.end {
            return None;
        }
        let mut buffer = String::new();
        let next_input = self.input.as_bytes()[self.start] as char;
        if next_input.is_numeric() {
            return Some(self.next_number(&mut buffer));
        }
        if next_input.is_whitespace() {
            self.inc();
            return Some(Token::Delimiter(DelimiterToken::Space));
        }
        if next_input == '"' || next_input == '\'' || next_input == '`' {
            return Some(self.next_string(&mut buffer, next_input));
        }
        if next_input.is_alphabetic() {
            return Some(self.next_identifier_or_keyword(&mut buffer));
        }
        let token = match next_input {
            '+' => Some(Token::Operator(OperatorToken::Plus)),
            '-' => Some(Token::Operator(OperatorToken::Minus)),
            '=' => Some(Token::Operator(OperatorToken::Assign)),
            ';' => Some(Token::Delimiter(DelimiterToken::SemiColon)),
            _ => Some(Token::Unknown(next_input)),
        };
        self.inc();
        return token;
    }
}

#[wasm_bindgen]
#[derive(Debug, Default)]
pub struct Lexer {
    input: String,
    start: usize,
    end: usize,
}

#[derive(Serialize)]
pub struct IteratedToken {
    value: Token,
    done: bool,
}

#[wasm_bindgen]
impl Lexer {
    pub fn new(input: &str) -> Self {
        let end = input.len();
        Self {
            input: input.to_string(),
            start: 0,
            end,
        }
    }
    fn inc(&mut self) {
        self.start = self.start + 1;
    }
    fn next_number(&mut self, buffer: &mut String) -> Token {
        let mut is_floating = false;
        loop {
            if self.start == self.end {
                break;
            };
            let maybe_next_input = self.input.chars().nth(self.start);
            if let Some(next_char) = maybe_next_input {
                if next_char.is_numeric() {
                    buffer.push(next_char);
                    self.inc();
                } else if next_char == '.' || next_char == ',' {
                    if is_floating {
                        panic!("cant have more than one decimal");
                    }
                    is_floating = true;
                    buffer.push(next_char);
                    self.inc();
                } else {
                    break;
                }
            }
        }
        if is_floating {
            return Token::Literal(LiteralToken::Number(NumberToken::Float(
                buffer.parse::<f64>().unwrap(),
            )));
        } else {
            return Token::Literal(LiteralToken::Number(NumberToken::Integer(
                buffer.parse::<i64>().unwrap(),
            )));
        }
    }
    fn next_string(&mut self, buffer: &mut String, quote_type: char) -> Token {
        self.inc();
        loop {
            if self.start == self.end {
                break;
            };
            let maybe_next_input = self.input.chars().nth(self.start);
            if let Some(next_char) = maybe_next_input {
                if next_char != quote_type {
                    buffer.push(next_char);
                    self.inc();
                } else {
                    self.inc();
                    break;
                }
            }
        }
        return Token::Literal(LiteralToken::String(buffer.to_string()));
    }
    fn next_identifier_or_keyword(&mut self, buffer: &mut String) -> Token {
        loop {
            if self.start == self.end {
                break;
            };
            let maybe_next_input = self.input.chars().nth(self.start);
            if let Some(next_char) = maybe_next_input {
                if matches!(next_char, 'a'..='z' | '0'..='9' | '_' | '-') {
                    buffer.push(next_char);
                    self.inc();
                } else {
                    break;
                }
            }
        }
        let token = if KEYWORDS.contains(&buffer.as_str()) {
            Token::Keywords(KeywordToken::from(buffer.to_string()))
        } else {
            Token::Identifier(IdentifierToken {
                name: buffer.to_string(),
            })
        };
        return token;
    }
    #[wasm_bindgen(js_name = next)]
    pub fn next_token(&mut self) -> JsValue {
        let maybe_token = self.next();
        return if let Some(token) = maybe_token {
            let it = IteratedToken {
                value: token,
                done: false,
            };
            serde_wasm_bindgen::to_value(&it).unwrap()
        } else {
            let it = IteratedToken {
                value: Token::End,
                done: true,
            };
            serde_wasm_bindgen::to_value(&it).unwrap()
        };
    }
}
